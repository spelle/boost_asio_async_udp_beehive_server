//
// server.cpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2015 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#define BOOST_LOG_DYN_LINK 1

#include <ctime>
#include <cstdlib>

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>

#include <boost/array.hpp>
#include <boost/asio.hpp>

#include <boost/log/common.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/core/null_deleter.hpp>
#include <boost/shared_ptr.hpp>

#include <sqlite3.h>

#include "beehiveFieldProtocol.h"



#define SESSIONKEY_LIFETIME 5.0




using namespace boost::log ;
using namespace boost::asio ;

using boost::asio::ip::udp ;



typedef enum
{
	initState,

	serverLogOnRequestReceive,
	serverLogOnRequestComplete,
	serverLogOnRequestValid,
	serverLogOnResponseBuild,
	serverLogOnResponseSend,
	serverLogOnResponseSent,

	serverBeehiveDataRequestReceive,
	serverBeehiveDataRequestComplete,
	serverBeehiveDataRequestValid,
	serverBeehiveDataResponseBuild,
	serverBeehiveDataResponseSend,
	serverBeehiveDataResponseSent,

	serverLogOffRequestReceive,
	serverLogOffRequestComplete,
	serverLogOffRequestValid,
	serverLogOffResponseBuild,
	serverLogOffResponseSend,
	serverLogOffResponseSent,

	endState
} udpBeehiveServerStateMachine_t ;



template<class T, std::size_t N>
void TRACE( sources::logger * l, boost::array<T, N> *array )
{
	std::ostringstream s ;
	for ( auto it : *array )
	{
		s << std::hex << std::setw(2) << std::setfill('0') << (short) it ;
		s << ' ';
	}

	BOOST_LOG( (*l) ) << "* TRACE : " << s.str() ;
}



template<class T>
void TRACE_HEX( sources::logger * l, const char * m, T & t )
{
	std::ostringstream s ;
	s << std::hex << "0x" << std::setw(2) << std::setfill('0') << (int) t ;
	s << ' ';

	BOOST_LOG( (*l) ) << "* TRACE : " << m << s.str() ;
}



uint16_t crc_ccitt_update (uint16_t crc, uint8_t data)
{
	data ^= (crc & 0x00FF) ;
	data ^= data << 4;

	return ((((uint16_t)data << 8) | (crc&0xFF00)) ^ (uint8_t)(data >> 4) ^ ((uint16_t)data << 3));
}



static int insert_callback(void *NotUsed, int argc, char **argv, char **azColName)
{
	int i;
	for(i=0; i<argc; i++)
	{
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}



int main()
{
	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;
	std::ostringstream sql ;

	typedef sinks::asynchronous_sink<sinks::text_ostream_backend> text_sink ;
	boost::shared_ptr<text_sink> sink = boost::make_shared<text_sink>() ;

	boost::shared_ptr<std::ostream> stream{&std::clog, boost::null_deleter{} } ;
	sink->locked_backend()->add_stream(stream) ;

	core::get()->add_sink(sink) ;

	sources::logger lg ;

	/* Open database */
	rc = sqlite3_open( "beehives.db", &db ) ;
	if( rc ) {
		BOOST_LOG(lg) << "Can't open database: " << sqlite3_errmsg(db) ;
		exit( 1 ) ;
	}
	else {
		BOOST_LOG(lg) << "Opened database successfully" ;
	}

	BOOST_LOG(lg) << "boost_asio_async_udp_beehive_server started" ;

	try {

		// Create an UDP socket
		boost::asio::io_service io_service ;
		udp::socket socket( io_service, udp::endpoint(udp::v4(), BEEHIVE_PROTOCOL_UDP_PORT) ) ;

		// A buffer for incoming requests
		boost::array<unsigned char, SERVER_REQUEST_SIZE> requestBuff ;
		serverLogOnRequest_t      * pServerLogOnRequest      =      (serverLogOnRequest_t *) requestBuff.data() ;
		beehiveDataFrameRequest_t * pBeehiveDataFrameRequest = (beehiveDataFrameRequest_t *) requestBuff.data() ;
		serverLogOffRequest_t     * pServerLogOffRequest     =     (serverLogOffRequest_t *) requestBuff.data() ;

		memset( requestBuff.data(), 0x00, requestBuff.max_size() ) ;

		// A buffer for outgoing responses
		boost::array<unsigned char, SERVER_RESPONSE_SIZE> responseBuff ;
		serverLogOnResponse_t      * pServerLogOnResponse      =      (serverLogOnResponse_t *) responseBuff.data() ;
		beehiveDataFrameResponse_t * pBeehiveDataFrameResponse = (beehiveDataFrameResponse_t *) responseBuff.data() ;
		serverLogOffResponse_t     * pServerLogOffResponse     =     (serverLogOffResponse_t *) responseBuff.data() ;

		memset( responseBuff.data(), 0x00, responseBuff.max_size() ) ;

		// A map for registering session keys
		std::map<sessionKey_t,time_t> sessionKeys ;

		for (;;) {
			udp::endpoint remote_endpoint ;

			boost::system::error_code error ;

			/* initialize buffers */
			memset( requestBuff.data(), 0x00, requestBuff.max_size() ) ;
			memset( responseBuff.data(), 0x00, responseBuff.max_size() ) ;

			bool responseToSend = false ;

			BOOST_LOG(lg) << "Receiving..." << std::endl ;
			socket.receive_from( boost::asio::buffer(requestBuff), remote_endpoint, 0, error ) ;

			if ( error && error != boost::asio::error::message_size )
				throw boost::system::system_error( error ) ;

			TRACE<unsigned char, SERVER_REQUEST_SIZE>(&lg, &requestBuff ) ;

			/* calculate CRC of request */
			CRC_t requestCRC = 0xffff ;
			for( uint8_t i = 0 ; i < requestBuff.data()[0] - sizeof(CRC_t) ; i ++ )
				requestCRC = crc_ccitt_update( requestCRC, requestBuff.data()[i] ) ;

			// TODO : change call to ctime into std::time call
			time_t timestamp = time(NULL) ;
			BOOST_LOG(lg) << ctime(&timestamp) ;

			if	// handle logOn Request
					(
						pServerLogOnRequest->frameSize  == sizeof(serverLogOnRequest_t) &&
						pServerLogOnRequest->serviceId  == logOn &&
						pServerLogOnRequest->CRC        == htons(requestCRC)
					) {
				/* A valid logOn request has been received */

				/* Make some trace of this */
				BOOST_LOG(lg) << "serverLogOnRequest has been received..." ;
				frameSize_t frameSize = pServerLogOnRequest->frameSize ;
				TRACE_HEX<frameSize_t>(       &lg, "serverLogOnRequest.frameSize       : ", frameSize ) ;
				serviceId_t serviceId = pServerLogOnRequest->serviceId ;
				TRACE_HEX<serviceId_t>(       &lg, "serverLogOnRequest.serviceId       : ", serviceId ) ;
				collectorId_t collectorId = ntohl(pServerLogOnRequest->collectorId) ;
				TRACE_HEX<collectorId_t>(     &lg, "serverLogOnRequest.collectorId     : ", collectorId ) ;
				protocolVersion_t protocolVersion = ntohs(pServerLogOnRequest->protocolVersion) ;
				TRACE_HEX<protocolVersion_t>( &lg, "serverLogOnRequest.protocolVersion : ", protocolVersion ) ;
				CRC_t CRC = ntohs(pServerLogOnRequest->CRC) ;
				TRACE_HEX<CRC_t>(             &lg, "serverLogOnRequest.CRC             : ", CRC ) ;

				/* Log it in DB as well */

				/* Create SQL statement */
				sql.str("") ;
				sql	<< "INSERT INTO Collectors( ColletorID, LogOnTimeStamp ) VALUES (" << collectorId << "," << timestamp << ");" ;
				BOOST_LOG(lg) << "SQL request : " << sql.str().c_str() ;
				/* Execute SQL statement */
				rc = sqlite3_exec(db, sql.str().c_str(), insert_callback, 0, &zErrMsg);
				if( rc != SQLITE_OK ) {
					BOOST_LOG(lg) << "SQL error: " << zErrMsg ;
					sqlite3_free(zErrMsg);
				}
				else {
					BOOST_LOG(lg) << "Records created successfully" ;
				}

				/* Prepare the response */

				pServerLogOnResponse->frameSize  = sizeof(serverLogOnResponse_t) ;
				pServerLogOnResponse->serviceId  = logOn_Ack ;

				/* Generate sessionKey */
				sessionKey_t sessionKey = 0x00000000 ;

				/* initialize random seed: */
				srand ( timestamp );

				/* generate secret number between 1 and 10: */
				sessionKey = rand() % 255 ;
				sessionKey <<= 8 ;
				sessionKey += rand() % 255 ;
				sessionKey <<= 8 ;
				sessionKey += rand() % 255 ;
				sessionKey <<= 8 ;
				sessionKey += rand() % 255 ;

				/* register the session key */
				sessionKeys.insert ( std::pair<sessionKey_t,time_t>( sessionKey, timestamp ) );

				/* fill the response buffer */
				pServerLogOnResponse->sessionKey = htonl(sessionKey) ;
				pServerLogOnResponse->errorCode  = success ;

				/* tell that there is a response to send */
				responseToSend = true ;
			}
			else if // handle beehiveDataFrame Request
					(
						pBeehiveDataFrameRequest->frameSize  == sizeof(beehiveDataFrameRequest_t) &&
						pBeehiveDataFrameRequest->serviceId  == beehiveDataFrame &&
						pBeehiveDataFrameRequest->CRC        == htons(requestCRC)
					) {
				/* A valid logOn request has been received */

				/* Make some trace of this */
				BOOST_LOG(lg) << "beehiveReadDataRequest has been received... " << sizeof(beehiveDataFrameRequest_t) ;
				TRACE_HEX<frameSize_t>(       &lg, "beehiveReadDataRequest.frameSize       : ", pBeehiveDataFrameRequest->frameSize ) ;
				TRACE_HEX<serviceId_t>(       &lg, "beehiveReadDataRequest.serviceId       : ", pBeehiveDataFrameRequest->serviceId ) ;
				sessionKey_t sessionKey = ntohl(pBeehiveDataFrameRequest->sessionKey) ;
				TRACE_HEX<sessionKey_t>(      &lg, "beehiveReadDataRequest.sessionKey      : ", sessionKey ) ;

				CRC_t CRC = ntohs(pBeehiveDataFrameRequest->CRC) ;
				TRACE_HEX<CRC_t>(             &lg, "beehiveReadDataRequest.CRC             : ", CRC ) ;

				/* Start filling response */

				pBeehiveDataFrameResponse->frameSize  = sizeof(beehiveDataFrameResponse_t) ;
				pBeehiveDataFrameResponse->serviceId  = beehiveDataFrame_Ack ;

				/* Search for session key */
				auto searchedKey = sessionKeys.find(sessionKey) ;
				if ( searchedKey == sessionKeys.end() ) {
					/* session key could not be found */
					pBeehiveDataFrameResponse->sessionKey = htonl(0x00000000) ;
					pBeehiveDataFrameResponse->errorCode  = badSessionKey ;
				}
				else {
					if ( SESSIONKEY_LIFETIME < difftime( searchedKey->second, timestamp ) ) {
						/* session key has been found, but has expired meanwhile */
						pBeehiveDataFrameResponse->sessionKey = htonl(0x00000000) ;
						pBeehiveDataFrameResponse->errorCode  = sessionKeyExpired ;
						sessionKeys.erase( searchedKey ) ;
					}
					else {
						/* session key has been found and is still valid */
						pBeehiveDataFrameResponse->sessionKey = htonl(searchedKey->first) ;
						pBeehiveDataFrameResponse->errorCode  = success ;

						/* Push data into DB */

						/* Create SQL statement */
						sql.str("") ;
						sql	<< "INSERT	INTO BeehivesData( " <<
												"BeehiveID," <<
												"TimeStamp," <<
												"gaugeNo1Value," <<
												"gaugeNo1RawValue," <<
												"gaugeNo2Value," <<
												"gaugeNo2RawValue," <<
												"gaugeNo3Value," <<
												"gaugeNo3RawValue," <<
												"scaleValue," <<
												"scaleRawValue )" <<
										"VALUES (" <<
												ntohl(pBeehiveDataFrameRequest->beehiveId) << "," <<
												timestamp << "," <<
												ntohs(pBeehiveDataFrameRequest->gaugeNo1Value) << "," <<
												ntohs(pBeehiveDataFrameRequest->gaugeNo1RawValue) << "," <<
												ntohs(pBeehiveDataFrameRequest->gaugeNo2Value) << "," <<
												ntohs(pBeehiveDataFrameRequest->gaugeNo2RawValue) << "," <<
												ntohs(pBeehiveDataFrameRequest->gaugeNo2Value) << "," <<
												ntohs(pBeehiveDataFrameRequest->gaugeNo3RawValue) << "," <<
												ntohs(pBeehiveDataFrameRequest->scaleValue) << "," <<
												ntohs(pBeehiveDataFrameRequest->scaleRawValue) << ");" ;
						BOOST_LOG(lg) << "SQL request : " << sql.str().c_str() ;
						/* Execute SQL statement */
						rc = sqlite3_exec(db, sql.str().c_str(), insert_callback, 0, &zErrMsg);
						if( rc != SQLITE_OK ) {
							BOOST_LOG(lg) << "SQL error: " << zErrMsg ;
							sqlite3_free(zErrMsg);
						}
						else {
							BOOST_LOG(lg) << "Records created successfully" ;
						}
					}
				}

				responseToSend = true ;
			}
			else if // handle logOff Request
					(
						pServerLogOffRequest->frameSize  == sizeof(serverLogOffRequest_t) &&
						pServerLogOffRequest->serviceId  == logOff &&
						//pServerLogOffRequest->sessionKey == htonl(0x12345678) &&
						pServerLogOffRequest->CRC        == htons(requestCRC)
					) {
				/* A valid logOn request has been received */

				/* Make some trace of this */

				BOOST_LOG(lg) << "serverLogOffRequest has been received..." ;
				TRACE_HEX<frameSize_t>(       &lg, "serverLogOffRequest.frameSize       : ", pServerLogOffRequest->frameSize ) ;
				TRACE_HEX<serviceId_t>(       &lg, "serverLogOffRequest.serviceId       : ", pServerLogOffRequest->serviceId ) ;
				sessionKey_t sessionKey = ntohl(pServerLogOffRequest->sessionKey) ;
				TRACE_HEX<sessionKey_t>(      &lg, "serverLogOffRequest.sessionKey     : ", sessionKey ) ;
				CRC_t CRC = ntohs(pServerLogOffRequest->CRC) ;
				TRACE_HEX<CRC_t>(             &lg, "serverLogOffRequest.CRC             : ", CRC ) ;

				/* Start filling response */
				pServerLogOffResponse->frameSize  = (sizeof(serverLogOffResponse_t)) ;
				pServerLogOffResponse->serviceId  = (logOff_Ack) ;

				/* Search for session key */
				auto searchedKey = sessionKeys.find(sessionKey) ;
				if ( searchedKey == sessionKeys.end() ) {
					/* session key could not be found */
					pBeehiveDataFrameResponse->sessionKey = htonl(0x00000000) ;
					pBeehiveDataFrameResponse->errorCode  = badSessionKey ;
				}
				else {
					if ( SESSIONKEY_LIFETIME < difftime( searchedKey->second, timestamp ) ) {
						/* session key has been found, but has expired meamwhile */
						pBeehiveDataFrameResponse->sessionKey = htonl(0x00000000) ;
						pBeehiveDataFrameResponse->errorCode  = sessionKeyExpired ;
					}
					else {
						/* session key has been found and is still valid */
						pBeehiveDataFrameResponse->sessionKey = htonl(searchedKey->first) ;
						pBeehiveDataFrameResponse->errorCode  = success ;
					}

					sessionKeys.erase( searchedKey ) ;
				}

				responseToSend = true ;
			}

			/* Check if there is a response to send */
			if ( true == responseToSend ) {

				/* calculate CRC of request */
				CRC_t responseCRC = 0xffff ;
				for( uint8_t i = 0 ; i < responseBuff.data()[0] - sizeof(CRC_t) ; i ++ )
					responseCRC = crc_ccitt_update( responseCRC, responseBuff.data()[i] ) ;

				/* put response CRC into response */
				*(CRC_t*)(&responseBuff.data()[responseBuff.data()[0] - sizeof(CRC_t)]) = htons(responseCRC) ;

				/* Log the response frame */
				TRACE<unsigned char, SERVER_RESPONSE_SIZE>(&lg, &responseBuff ) ;

				/* send the response */
				boost::system::error_code ignored_error;
				socket.send_to(boost::asio::buffer(responseBuff), remote_endpoint, 0, ignored_error);
			}
		}
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}

	sqlite3_close(db);

	return 0;
}
